export const listenShowMore = callback => {
    document
        .querySelector('.js-planets-list')
        .addEventListener(
            'click',
            e => {
                if (e.target.classList.contains('btn')) {
                    const filmIds = e.target.getAttribute('data-films-ids').split(',');
                    callback(filmIds);
                }
            }
        )
}
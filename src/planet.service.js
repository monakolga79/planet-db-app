export class PlanetService {
    static async get(page) {
        const responce = await fetch('http://swapi.dev/api/planets');
        const data = await responce.json();
        return data.results.map(({ name, diameter, population, gravity, terrain, climate, films }) => ({ name, diameter, population, gravity, terrain, climate, films }));
    }
};        
export class PlanetListRender {
    static renderPage(planets){
        console.log(planets);
        const planetListPage = document.querySelector('.js-planets-list');
        planetListPage.innerHTML = '';
        planets.forEach(planet => planetListPage.append(PlanetListRender.getCard(planet))); 
    }
    
    static getCard(planet) {
        const col = document.createElement('div');
        col.classList.add('col-md-3');

        const card = document.createElement('div');
        card.classList.add('card');

        const img = document.createElement('img')
        img.setAttribute('src', './img/planets.jpg');
        img.classList.add('card-img-top');

        const cardBody = document.createElement('div')
        cardBody.classList.add('card-body');

        const cardTitle = document.createElement('h5');
        cardTitle.classList.add('card-title');
        cardTitle.textContent = planet.name;

        const cardText = document.createElement('p');
        cardText.classList.add('card-text');

        const listGroup = document.createElement('ul');
        listGroup.classList.add('list-group', 'list-group-flush');

        [
            { propName: 'diameter', dispName: 'Диаметр' },
            { propName: 'population', dispName: 'Население' },
            { propName: 'gravity', dispName: 'Гравитация' },
            { propName: 'terrain', dispName: 'Природа' },
            { propName: 'climate', dispName: 'Климат' },
        ].forEach(({ propName, dispName }) => {

            const listItem = document.createElement('li');
            listItem.classList.add('list-group-item');

            const spanDispName = document.createElement('span');
            spanDispName.textContent = dispName;

            const spanPropName = document.createElement('span');
            spanPropName.textContent = planet[propName];

            listItem.append(spanDispName);
            listItem.append(spanPropName);

            listGroup.append(listItem);
        });

        const btnPlanet = document.createElement('a');
        btnPlanet.classList.add('btn', 'btn-primary');
        btnPlanet.setAttribute('data-bs-toggle', 'modal');
        btnPlanet.setAttribute('data-bs-target', '#staticBackdrop');
        btnPlanet.textContent = 'Узнать больше';

        const filmId = [];
        planet.films.forEach(film => {
            const id = film.split('/');
            filmId.push(id[id.length - 2]);
        });
        btnPlanet.setAttribute('data-films-ids', filmId.join(','));

        cardText.append(listGroup);
        cardBody.append(cardTitle);
        cardBody.append(cardText);
        card.append(img);
        card.append(cardBody);
        card.append(btnPlanet);
        col.append(card);

        return col;
    }
};